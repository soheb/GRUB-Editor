#-------------------------------------------------
#
# Project created by QtCreator 2012-10-21T15:45:02
#
#-------------------------------------------------

QT       += core gui

TARGET = GrubEditor
TEMPLATE = app


SOURCES += main.cpp\
        grubeditor.cpp

HEADERS  += grubeditor.h

FORMS    += grubeditor.ui
