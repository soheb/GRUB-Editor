#include "grubeditor.h"
#include "ui_grubeditor.h"

GrubEditor::GrubEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GrubEditor)
{
    ui->setupUi(this);
}

GrubEditor::~GrubEditor()
{
    delete ui;
}
