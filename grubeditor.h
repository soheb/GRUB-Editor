#ifndef GRUBEDITOR_H
#define GRUBEDITOR_H

#include <QMainWindow>

namespace Ui {
class GrubEditor;
}

class GrubEditor : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit GrubEditor(QWidget *parent = 0);
    ~GrubEditor();
    
private:
    Ui::GrubEditor *ui;
};

#endif // GRUBEDITOR_H
